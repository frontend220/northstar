import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { http } from Http

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  insurance_types:any;

  constructor(private router: Router,private http: HttpClient) {
    this.getJSON().subscribe(data => {
      this.insurance_types = data.product.product_list.product_list_item;
      console.log("JSONData: ",data.product.product_list.product_list_item);
  });
  }

  ngOnInit(): void {
  }

  productSelect(type){
    console.log("type: ",type);
    this.router.navigate(['/product/'],{
        state: type,
    });
  }
  getJSON(): Observable<any> {
    return this.http.get("https://xmltojsonfiles.s3.amazonaws.com/product.json");
  }

}
