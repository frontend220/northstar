import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ModalPageComponent } from '../modal-page/modal-page.component';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import JSONData from '../../../assets/childrens_whole_life.json';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit {
  status: boolean = true;
  simpleform: any;
  sections: any;
  fields: any;
  selectedProduct: any;

  myForm: FormGroup;
  advancedForm = JSONData

  constructor(public modalController: ModalController,
    private fb: FormBuilder,
    private alertCtrl: AlertController,
    private router: Router,
    private http: HttpClient) {

    this.selectedProduct = this.router.getCurrentNavigation().extras?.state;

    this.getJSON(this.selectedProduct.product_page_url).subscribe(data => {
      // this.advancedForm = data;
      console.log("JSONData",data);
    });

    this.myForm = this.fb.group({});
    this.createForms(this.advancedForm?.formTypes);

  }

  ngOnInit() {
  }

  createForms(forms: any) {
    for(let form of forms){
        console.log(form);
        const newFormcontrol = new FormControl();
        this.myForm.addControl(form.name, newFormcontrol);
    }
    this.sections = forms[0]?.sections;
    for(let section of this.sections) {
      this.createControls(section.fields);
    }
  }

  createControls(controls: any ) {
    for (let conntrol of controls) {
      const newFormcontrol = new FormControl();
      // if (conntrol.options.required) {
      //   newFormcontrol.setValidators(Validators.requiredTrue);
      // }
      this.myForm.addControl(conntrol.key,newFormcontrol);
    }
    console.log("myform",this.myForm);
  }

  async submitForm() {
    const modal = await this.modalController.create({
      component: ModalPageComponent,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  selectSection(sections:any){
    for(let form of this.advancedForm.formTypes) {
        // This will work for 2 buttons only
        form.status = !form.status
    }
    this.sections = sections;
    for(let section of this.sections) {
      this.createControls(section.fields);
    }
  }

  backButtonClick() {
    this.router.navigate(['/home/']);
  }

  getJSON(product_page_url:any): Observable<any> {
    return this.http.get("https://xmltojsonfiles.s3.amazonaws.com/"+product_page_url);
  }

}
