import os 
from xml.etree import ElementTree as ET
import re, collections
from configparser import ConfigParser
import xmltodict, json, tinys3
from colored import fg

import pyfiglet
  
config = ConfigParser()
config.read('config.ini')
xmlFilePath = os.path.abspath(config['inputData']['xmlFilePath'])
ACCESS_KEY = config['inputData']['accessKeyID']
SECRET_KEY = config['inputData']['accessSecretKey']

result = pyfiglet.figlet_format("XML To JSON Convertor")
color = fg('red')
print(color + result)

class Solution:
    def __init__(self):
        self.fileName = 'product.json'

    def traverseXML(self, node):
        if not node: return None
        for child in node:
            if child.tag == 'product_page_url':
                self.fileName = child.text
                continue
            if child.tag == 'formTypes':
                self.getJsonFile(child, False, self.fileName)
                continue
            self.traverseXML(child)
    
    def getJsonFile(self, xmlNode, isDeleteRequired = True, fileName = 'product.json'):
        color = fg('blue')
        print(color + '----------------------------- Creating', f'File - {self.fileName}')
        xmlString = ET.tostring(xmlNode, encoding='utf-8', method='xml')
        xmlDict = xmltodict.parse(xmlString)
        if isDeleteRequired:
            for hashMap in xmlDict['product']['product_list']['product_list_item']:
                if 'formTypes' in hashMap:
                    del hashMap['formTypes']

        jsonData = json.dumps(xmlDict)
        with open(f"{fileName}", "w") as json_file:
            json_file.write(jsonData)
            json_file.close()

        self.uploadFiletoS3(fileName)
    
    def uploadFiletoS3(self, fileName):
        global ACCESS_KEY, SECRET_KEY
        conn = tinys3.Connection(ACCESS_KEY, SECRET_KEY, tls=True)
        f = open(f'{fileName}', 'rb')
        color = fg('blue')
        print(color + '----------------------------- Uploading to S3', f'File - {self.fileName}', end='')
        conn.upload(f'{fileName}', f, 'xmltojsonfiles')
        color = fg('green')
        print(color + ' ...done \n')

color = fg('cyan')
print(color + '---- Parsing XML File -', xmlFilePath)
dom = ET.parse(xmlFilePath)
root = dom.getroot()
xmlString = ET.tostring(root, encoding='unicode', method='xml')
obj = Solution()
obj.getJsonFile(root)
obj.traverseXML(root)

