declare module namespace {

    export interface Field {
        key: string;
        type: string;
        label: string;
        placeholder: string;
        default: any;
        options: string[];
        minValue?: number;
        maxValue?: number;
    }

    export interface Section {
        sectionName: string;
        fields: Field[];
    }

    export interface FormType {
        name: string;
        sections: Section[];
    }

    // export interface RootObject {
    //     formTypes: FormType[];
    // }

}

